package com.example.libros.mapper;

import com.example.libros.dto.AuthorDto;
import com.example.libros.entity.Author;
import org.springframework.stereotype.Component;

@Component
public class AuthorMapper {
    public Author fromDto(AuthorDto dto) {
        Author author = new Author();
        author.setName(dto.getName());
        author.setCountry(dto.getCountry());
        author.setGender(dto.getGender());
        author.setDeadYear(dto.getDeadYear());
        author.setBirthYear(dto.getBirthYear());
        return author;
    }

    public Author fromDto(Author author, AuthorDto dto) {
        author.setName(dto.getName());
        author.setCountry(dto.getCountry());
        author.setGender(dto.getGender());
        author.setDeadYear(dto.getDeadYear());
        author.setBirthYear(dto.getBirthYear());
        return author;
    }
}
