package com.example.libros.mapper;

import com.example.libros.dto.BookDto;
import com.example.libros.entity.Book;
import org.springframework.stereotype.Component;

@Component
public class BookMapper {
    public Book fromDto(BookDto dto) {
        Book book = new Book();
        book.setName(dto.getName());
        book.setPublicationYear(dto.getPublicationYear());
        return book;
    }

    public Book fromDto(Book book, BookDto dto) {
        book.setName(dto.getName());
        book.setPublicationYear(dto.getPublicationYear());
        return book;
    }
}
