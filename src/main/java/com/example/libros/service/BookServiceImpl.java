package com.example.libros.service;

import com.example.libros.dto.BookDto;
import com.example.libros.entity.Author;
import com.example.libros.entity.Book;
import com.example.libros.exception.EntityNotFoundException;
import com.example.libros.mapper.BookMapper;
import com.example.libros.repository.BookRespository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
public class BookServiceImpl implements BookService {
    private BookRespository bookRespository;
    private BookMapper bookMapper;
    private AuthorService authorService;

    public List<Book> getAll() {
        return bookRespository.findByIsDeletedFalse();
    }

    @Override
    public Book getById(UUID id) {
        return bookRespository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Book", id));
    }

    @Override
    public List<Book> getByPublicationYear(int year) {
        return bookRespository.findByPublicationYear(year);
    }

    @Override
    public Book insert(BookDto dto) {
        Author authorFound = authorService.getById(dto.getIdAuthor());
        Book book = bookMapper.fromDto(dto);
        book.setAuthor(authorFound);
        return bookRespository.save(book);
    }

    @Override
    public Book update(UUID id, BookDto dto) {
        Author author = authorService.getById(dto.getIdAuthor());
        Book bookFound = bookRespository.getById(id);
        Book bookMapped = bookMapper.fromDto(bookFound, dto);
        bookMapped.setAuthor(author);
        return bookRespository.save(bookMapped);
    }

    @Override
    public void delete(UUID id) {
        Book bookFound = bookRespository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Book", id));
        bookFound.setIsDeleted(true);
        bookRespository.save(bookFound);
    }
}
