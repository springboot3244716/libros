package com.example.libros.service;

import com.example.libros.dto.BookDto;
import com.example.libros.entity.Book;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

public interface BookService {
    List<Book> getAll();
    Book getById(UUID id);
    List<Book> getByPublicationYear(int year);
    Book insert(BookDto dto);
    Book update(UUID id, BookDto dto);
    void delete(UUID id);
}
