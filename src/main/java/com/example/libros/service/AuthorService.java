package com.example.libros.service;

import com.example.libros.dto.AuthorDto;
import com.example.libros.entity.Author;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

public interface AuthorService {
    List<Author> getAll();
    Author getById(UUID id);
    List<Author> findByName(String name);
    List<Author> findByBirthYear(LocalDate birthYear);
    Author insert(AuthorDto dto);
    Author update(UUID id, AuthorDto dto);
    void delete(UUID id);
}
