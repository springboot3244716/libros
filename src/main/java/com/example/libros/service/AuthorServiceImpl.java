package com.example.libros.service;

import com.example.libros.dto.AuthorDto;
import com.example.libros.entity.Author;
import com.example.libros.exception.EntityNotFoundException;
import com.example.libros.mapper.AuthorMapper;
import com.example.libros.repository.AuthorRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
public class AuthorServiceImpl implements AuthorService {
    private AuthorRepository authorRepository;
    private AuthorMapper authorMapper;

    @Override
    public List<Author> getAll() {
        return authorRepository.findByIsDeletedFalse();
    }

    @Override
    public Author getById(UUID id) {
        return authorRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Author", id));
    }

    @Override
    public List<Author> findByName(String name) {
        return authorRepository.findByName(name);
    }

    @Override
    public List<Author> findByBirthYear(LocalDate birthYear) {
        return authorRepository.findByBirthYear(birthYear);
    }

    @Override
    public Author insert(AuthorDto dto) {
        Author author = authorMapper.fromDto(dto);
        return authorRepository.save(author);
    }

    @Override
    public Author update(UUID id, AuthorDto dto) {
        Author authorFound = authorRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Author", id));
        Author authorMapped = authorMapper.fromDto(authorFound, dto);
        return authorRepository.save(authorMapped);
    }

    @Override
    public void delete(UUID id) {
        Author authorFound = authorRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Author", id));
        authorFound.setIsDeleted(true);
        authorRepository.save(authorFound);
    }
}
