package com.example.libros.entity;

import jakarta.persistence.*;

import java.sql.Types;
import java.time.LocalDate;
import java.util.UUID;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.JdbcTypeCode;


//@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "authors")
public class Author {

    @Id
    @GeneratedValue
    @JdbcTypeCode(Types.VARCHAR)
    private UUID id;

    private String name;

    @Column(name = "birth_year")
    private LocalDate birthYear;

    @Column(name = "dead_year")
    private LocalDate deadYear;

    private String country;

    @Enumerated(value = EnumType.STRING)
    private GenerEnum gender = GenerEnum.M;

    @JdbcTypeCode(Types.BOOLEAN)
    @Column(name = "is_deleted")
    private Boolean isDeleted = false; // Inicializar con false por defecto

    @PrePersist
    protected void onCreate() {
        if (isDeleted == null) {
            isDeleted = false;
        }
    }
}
