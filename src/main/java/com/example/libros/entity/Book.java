package com.example.libros.entity;


import jakarta.persistence.*;

import java.sql.Types;
import java.time.LocalDate;
import java.util.UUID;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.JdbcTypeCode;

@Entity
@NoArgsConstructor
@Getter
@Setter
@Table(name = "books")
public class Book {

    @Id
    @GeneratedValue
    @JdbcTypeCode(Types.VARCHAR)
    private UUID id;

    private String name;

    @Column(name = "publication_year")
    private Integer publicationYear;

    @JdbcTypeCode(Types.BOOLEAN)
    @Column(name = "is_deleted")
    private Boolean isDeleted;

    @PrePersist
    private void prePersistIsDeleted(){
        System.out.println("Agregamos");
        isDeleted = false;
    }

    @ManyToOne
    @JoinColumn(name = "id_author")
    private Author author;
}
