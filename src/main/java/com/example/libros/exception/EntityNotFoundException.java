package com.example.libros.exception;

import java.util.UUID;

public class EntityNotFoundException extends RuntimeException {
    public EntityNotFoundException(String message) {
        super(message);
    }

    public EntityNotFoundException(String entity, UUID id) {
        super(String.format("%a with id: %b not found.", entity, id));
    }
}
