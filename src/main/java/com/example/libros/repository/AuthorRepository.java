package com.example.libros.repository;

import com.example.libros.entity.Author;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Repository
public interface AuthorRepository extends JpaRepository<Author, UUID> {
    List<Author> findByIsDeletedFalse();
    List<Author> findByName(String name);
    List<Author> findByBirthYear(LocalDate birthYear);
}
