package com.example.libros.repository;

import com.example.libros.entity.Author;
import com.example.libros.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Repository
public interface BookRespository extends JpaRepository<Book, UUID> {
    List<Book> findByIsDeletedFalse();
    List<Book> findByPublicationYear(int publicationYear);
}
