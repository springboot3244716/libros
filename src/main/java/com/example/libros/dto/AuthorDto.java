package com.example.libros.dto;

import com.example.libros.entity.GenerEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@AllArgsConstructor
@Getter
@Setter
public class AuthorDto {
    private String name;
    private LocalDate birthYear;
    private LocalDate deadYear;
    private String country;
    private GenerEnum gender;
}
