package com.example.libros.dto;

import com.example.libros.entity.Author;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.UUID;

@AllArgsConstructor
@Getter
@Setter
public class BookDto {
    private String name;
    private int publicationYear;
    private UUID idAuthor;
}
