package com.example.libros.controller;

import com.example.libros.dto.AuthorDto;
import com.example.libros.entity.Author;
import com.example.libros.service.AuthorService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@RestController
@RequestMapping("/author")
public class AuthorController {
    private AuthorService authorService;

    @GetMapping
    public ResponseEntity<List<Author>> getAll() {
        List<Author> authors = authorService.getAll();
        return ResponseEntity.status(HttpStatus.OK).body(authors);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Author> getById(@PathVariable UUID id) {
        Author authorFound = authorService.getById(id);
        return ResponseEntity.status(HttpStatus.OK).body(authorFound);
    }

    @GetMapping("/name/{name}")
    public ResponseEntity<List<Author>> findByName(@PathVariable String name) {
        List<Author> authors = authorService.findByName(name);
        return ResponseEntity.status(HttpStatus.OK).body(authors);
    }

    @GetMapping("/birth/{date}")
    public ResponseEntity<List<Author>> findByBirthDate(@PathVariable LocalDate date) {
        List<Author> authors = authorService.findByBirthYear(date);
        return ResponseEntity.status(HttpStatus.OK).body(authors);
    }

    @PostMapping
    public ResponseEntity<Author> insert(@RequestBody AuthorDto dto) {
        Author authorSaved = authorService.insert(dto);
        return ResponseEntity.status(HttpStatus.CREATED).body(authorSaved);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Author> update(@PathVariable UUID id, @RequestBody AuthorDto dto) {
        System.out.println("saludos");
        Author authorSaved = authorService.update(id, dto);
        return ResponseEntity.status(HttpStatus.OK).body(authorSaved);
    }

    @DeleteMapping("/{id}")
    public void deleted(@PathVariable UUID id) {
        authorService.delete(id);
    }
}
