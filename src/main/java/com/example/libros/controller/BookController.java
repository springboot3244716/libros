package com.example.libros.controller;

import com.example.libros.dto.AuthorDto;
import com.example.libros.dto.BookDto;
import com.example.libros.entity.Author;
import com.example.libros.entity.Book;
import com.example.libros.service.BookService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/book")
@AllArgsConstructor
public class BookController {
    private BookService bookService;

    @GetMapping
    public ResponseEntity<List<Book>> getAll(){
        List<Book> books = bookService.getAll();
        return ResponseEntity.status(HttpStatus.OK).body(books);
    }

    @GetMapping("/publication_year/{year}")
    public ResponseEntity<List<Book>> getAll(@PathVariable int year){
        List<Book> books = bookService.getByPublicationYear(year);
        return ResponseEntity.status(HttpStatus.OK).body(books);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Book> getById(@PathVariable UUID id) {
        Book bookFound = bookService.getById(id);
        return ResponseEntity.status(HttpStatus.OK).body(bookFound);
    }

    @PostMapping
    public ResponseEntity<Book> insert(@RequestBody BookDto dto) {
        Book bookSaved = bookService.insert(dto);
        return ResponseEntity.status(HttpStatus.CREATED).body(bookSaved);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Book> update(@PathVariable UUID id, @RequestBody BookDto dto) {
        Book bookSaved = bookService.update(id, dto);
        return ResponseEntity.status(HttpStatus.OK).body(bookSaved);
    }

    @DeleteMapping("/{id}")
    public void deleted(@PathVariable UUID id) {
        bookService.delete(id);
    }
}
